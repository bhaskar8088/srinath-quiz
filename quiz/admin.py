from django.contrib import admin

# Register your models here.
from quiz.models import Quiz, MCQuestion, SubjectiveQuestion, Choice, School, Student

admin.site.register(Quiz)
admin.site.register(MCQuestion)
admin.site.register(Choice)
admin.site.register(SubjectiveQuestion)
admin.site.register(School)
admin.site.register(Student)