from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponse
from django.http import Http404
from django.core.urlresolvers import reverse_lazy

# Create your views here.
from django.views import View
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import TemplateView, FormView, DetailView
from .mixins import AdminMixin, StudentMixin, TeacherMixin

from auth0.forms import TeacherRegisterForm
from quiz.forms import RegisterSchoolForm, NewQuizForm
from quiz.models import Quiz, Choice, AnswerSheet, School, Student


class HomeView(TemplateView):
  template_name = 'quiz/home.html'

  def get(self, request, *args, **kwargs):
    if hasattr(request.user, 'teacher'):
      return redirect('quiz:teacher-home')
    elif request.user.is_superuser:
      return redirect('quiz:admin-home')
    return super(HomeView, self).get(request, *args, **kwargs)


class AdminHomeView(AdminMixin, TemplateView):
  template_name = 'quiz/admin-home.html'


class TeacherHomeView(TeacherMixin, TemplateView):
  template_name = 'quiz/teacher-home.html'


class TeacherStudentView(TeacherMixin, DetailView):
  template_name = 'quiz/teacher-student.html'
  model = Student
  context_object_name = 'student'


class TeacherQuizView(TeacherMixin, TemplateView):
  template_name = 'quiz/teacher-quiz.html'

  def get_context_data(self, **kwargs):
    context = super(TeacherQuizView,self).get_context_data(**kwargs)
    quiz = get_object_or_404(Quiz, pk=kwargs['quiz_id'])
    context['quiz'] = quiz
    context['students'] = Student.objects.filter(id__in=AnswerSheet.objects.filter(quiz=quiz).values_list('student__id', flat=True))
    return context


class SubmitScoreView(TeacherMixin, View):

  def post(self, request, *args, **kwargs):
    answer = get_object_or_404(AnswerSheet, pk=request.POST.get('answer_id'))
    answer.score = request.POST.get('score')
    answer.save()
    return HttpResponse(content='', status=201)


class QuizQuestionView(StudentMixin, TemplateView):
  template_name = 'quiz/quiz-question.html'

  def get_context_data(self, **kwargs):
    quiz_id = kwargs['quiz_id']
    q_num = kwargs['question_num']

    question = get_object_or_404(Quiz, pk=quiz_id).get_question_by_num(q_num)

    if question['type'] == 'out_of_range':
      raise Http404

    context = {
      'question': question['object'],
      'is_mcq': question['type'] == 'mcq',
    }

    return context

  def post(self, request, *args, **kwargs):
    quiz_id = kwargs['quiz_id']
    q_num = int(kwargs['question_num'])

    quiz = get_object_or_404(Quiz, pk=quiz_id)
    question = get_object_or_404(Quiz, pk=quiz_id).get_question_by_num(q_num)

    if question['type'] == 'out_of_range':
      raise Http404
    elif question['type'] == 'mcq':
      answer = get_object_or_404(Choice, pk=request.POST.get('answer', -1))
    else:
      answer = request.POST.get('answer', 'answer_submission_error')

    request.user.student.submit_answer(question=question["object"], answer=answer)

    if q_num < quiz.get_num_of_questions():
      return redirect('quiz:quiz-question', quiz_id=kwargs['quiz_id'], question_num=(q_num + 1))

    request.user.student.set_quiz_uneditable(quiz)
    return redirect('quiz:quiz-result', quiz_id=kwargs['quiz_id'], student_id=request.user.student.id)


class QuizResultView(TemplateView):
  template_name = 'quiz/quiz_result.html'
  context_object_name = 'answer_sheet'

  def get(self, request, *args, **kwargs):
    return super(QuizResultView, self).get(request, *args, **kwargs)

  def get_context_data(self, **kwargs):
    quiz = get_object_or_404(Quiz, pk=self.kwargs['quiz_id'])
    student = get_object_or_404(Student, pk=self.kwargs['student_id'])
    context = {
      'answer_sheet': student.get_answer_sheet_for_quiz(quiz=quiz),
      'quiz': quiz,
      'score': student.get_score_for_quiz(quiz=quiz)
    }
    return context


class RegisterSchool(AdminMixin, CreateView):
  template_name = 'quiz/register-school.html'
  form_class = RegisterSchoolForm
  success_url = reverse_lazy('quiz:register-school')


class RegisterTeacher(AdminMixin, FormView):
  template_name = 'auth0/register-teacher.html'
  form_class = TeacherRegisterForm
  success_url = reverse_lazy('quiz:teacher-home')

  def get_context_data(self, **kwargs):
    context = super(RegisterTeacher, self).get_context_data()
    context['schools'] = School.objects.all().order_by('name')
    return context

  def form_valid(self, form):
    form.save()
    return super(RegisterTeacher, self).form_valid(form)


class NewQuiz(AdminMixin, FormView):
  template_name = 'quiz/new-quiz.html'
  form_class = NewQuizForm
  success_url = reverse_lazy('quiz:new-quiz')

  def post(self, request, *args, **kwargs):
    form_class = self.get_form_class()
    form = self.get_form(form_class)

    if form.is_valid():
      form.instance.created_by = request.user
      quiz = form.save()

      questions = request.FILES.get('questions', False)
      if questions:
        question_num = 1
        for line in questions:
          row = line.split(',')
          if len(row) == 2:
            quiz.add_subjective_question(question=row[0], score=row[1], question_num=question_num)
          elif len(row) > 2:
            quiz.add_mc_question(question=row[0], score=row[1], answer=row[2], choices=row[3:], question_num=question_num)

          question_num += 1

      return self.form_valid(form)
    else:
      return self.form_invalid(form)