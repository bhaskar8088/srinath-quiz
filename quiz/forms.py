from django import forms

from quiz.models import School, Quiz


class RegisterSchoolForm(forms.ModelForm):

  class Meta:
    model = School
    exclude = ('created_at', 'updated_at')

  def test_func(self):
    return self.requ


class NewQuizForm(forms.ModelForm):
  questions = forms.FileField(required=False, label='Upload Questions(*.csv)')

  class Meta:
    model = Quiz
    exclude = ('created_at', 'updated_at', 'created_by')