"""Auth0 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^$', views.HomeView.as_view(), name='home'),
  url(r'^register-school/$', views.RegisterSchool.as_view(), name='register-school'),
  url(r'^quiz/new/$', views.NewQuiz.as_view(), name='new-quiz'),

  url(r'^quiz/(?P<quiz_id>[0-9]+)/(?P<question_num>[0-9]+)/$', views.QuizQuestionView.as_view(), name='quiz-question'),
  url(r'^quiz/result/(?P<student_id>[0-9]+)/(?P<quiz_id>[0-9]+)/$', views.QuizResultView.as_view(), name='quiz-result'),

  url(r'^register-teacher/$', views.RegisterTeacher.as_view(), name='register-teacher'),
  url(r'^teacher/$', views.TeacherHomeView.as_view(), name='teacher-home'),
  url(r'^^teacher/student/(?P<pk>[0-9]+)/$', views.TeacherStudentView.as_view(), name='teacher-student'),
  url(r'teacher/quiz/(?P<quiz_id>[0-9]+)/$', views.TeacherQuizView.as_view(), name='teacher-quiz'),
  url(r'^teacher/submit-score/$', views.SubmitScoreView.as_view(), name='submit-score'),

  url(r'^adm/home', views.AdminHomeView.as_view(), name='admin-home'),
]
