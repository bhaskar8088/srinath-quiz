from django import template

register = template.Library()


@register.filter(name='get_score')
def get_score_for_quiz(student, quiz):
  return student.get_score_for_quiz(quiz)
