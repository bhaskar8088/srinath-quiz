from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

# Create your models here.
from django.db.models import Sum
from django.utils import timezone


class Quiz(models.Model):
  name = models.CharField(max_length=100)
  quiz_commence_date = models.DateTimeField()
  standard = models.IntegerField()

  created_by = models.ForeignKey(User)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)

  def get_num_of_questions(self):
    return MCQuestion.objects.filter(quiz=self).count()\
           + SubjectiveQuestion.objects.filter(quiz=self).count()

  def get_total_score(self):
    return MCQuestion.objects.filter(quiz=self).aggregate(score=Sum('score'))['score']\
           + SubjectiveQuestion.objects.filter(quiz=self).aggregate(score=Sum('score'))['score']

  def add_mc_question(self, question, choices, answer, question_num, score=1):
    inserted_choices = []
    inserted_answer = None

    for choice in choices:
      inserted_choice = Choice.objects.create(text=choice.strip())
      inserted_choices.append(inserted_choice)
      if choice == answer:
        inserted_answer = inserted_choice

    inserted_question = MCQuestion.objects.create(
      text=question.strip(),
      quiz=self,
      question_num=question_num,
      answer=inserted_answer,
      score=score
    )

    for choice in inserted_choices:
      inserted_question.choices.add(choice)

    return inserted_question

  def add_subjective_question(self, question, question_num, score=1):
    return SubjectiveQuestion.objects.create(
      quiz=self,
      question_num=question_num,
      text=question.strip(),
      score=score
    )

  def get_question_by_num(self, num):
    try:
      return {
        'type': 'mcq',
        'object': MCQuestion.objects.get(quiz=self, question_num=num),
      }
    except ObjectDoesNotExist:
      pass

    try:
      return {
        'type': 'sub',
        'object': SubjectiveQuestion.objects.get(quiz=self, question_num=num),
      }
    except ObjectDoesNotExist:
      pass

    return {
      'type': 'out_of_range',
      'object': None,
    }


class Choice(models.Model):
  text = models.TextField(max_length=500)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)


class MCQuestion(models.Model):
  quiz = models.ForeignKey(Quiz)
  question_num = models.IntegerField()

  text = models.TextField(max_length=1000)
  choices = models.ManyToManyField(Choice, related_name='question')
  answer = models.ForeignKey(Choice)

  score = models.IntegerField(default=1)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)


class SubjectiveQuestion(models.Model):
  quiz = models.ForeignKey(Quiz)
  question_num = models.IntegerField()

  text = models.TextField(max_length=1000)
  score = models.IntegerField(default=1)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)


class School(models.Model):
  name = models.CharField(max_length=100)

  address = models.TextField(max_length=400)
  contact = models.CharField(max_length=15)
  alt_contact = models.CharField(max_length=15, blank=True, null=True)
  email = models.EmailField()
  website = models.URLField(blank=True, null=True)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)


class Teacher(models.Model):
  user = models.OneToOneField(User, related_name='teacher')

  first_name = models.CharField(max_length=20)
  last_name = models.CharField(max_length=20)
  middle_name = models.CharField(max_length=20, blank=True, null=True)

  school = models.ForeignKey(School)
  standard = models.IntegerField()

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)

  def get_students(self):
    return Student.objects.filter(school=self.school).filter(standard=self.standard)

  def set_score(self, student, question, score=0):
    if student.school == self.school:
      try:
        answer = AnswerSheet.objects.get(
          student=student,
          content_type__model='subjectivequestion',
          object_id=question.id
        )
        if score > question.score:
          score = question.score
        answer.score = score
        answer.save()
        return answer
      except ObjectDoesNotExist:
        return None
    return None

  def get_past_quizzes(self):
    return Quiz.objects.filter(standard=self.standard)


class Student(models.Model):
  user = models.OneToOneField(User, related_name='student')

  first_name = models.CharField(max_length=20)
  last_name = models.CharField(max_length=20)
  middle_name = models.CharField(max_length=20, blank=True, null=True)

  school = models.ForeignKey(School)
  standard = models.IntegerField()
  roll_num = models.CharField(max_length=10, blank=True, null=True)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)

  def get_upcoming_quizzes(self):
    return Quiz.objects.filter(standard=self.standard)\
      .filter(quiz_commence_date__gt=timezone.now())

  def get_attempted_quizzes(self):
    return Quiz.objects.filter(id__in=AnswerSheet.objects.filter(student=self)
                               .values_list('quiz__id', flat=True))

  def get_active_quizzes(self):
    return Quiz.objects.filter(standard=self.standard)\
      .filter(quiz_commence_date__day=timezone.now().day)\
      .filter(quiz_commence_date__month=timezone.now().month)\
      .filter(quiz_commence_date__year=timezone.now().year)

  def get_score_for_quiz(self, quiz):
    return self.get_answer_sheet_for_quiz(quiz).aggregate(score=Sum('score'))['score']

  def get_answer_sheet_for_quiz(self, quiz):
    return AnswerSheet.objects.filter(quiz=quiz).filter(student=self)

  def get_unattempted_mcquestions_for_quiz(self, quiz):
    attempted_mc_questions = AnswerSheet.objects\
      .filter(questions__quiz=quiz)\
      .filter(student=self)\
      .filter(content_type__model='MCQuestion').values_list('id', flat=True)

    return MCQuestion.objects.filter(quiz=quiz).exclude(id__in=attempted_mc_questions)

  def submit_answer(self, question, answer):
    if isinstance(question, MCQuestion):
      if question.answer == answer:
        score = question.score
      else:
        score = 0

      try:
        a_sheet = AnswerSheet.objects.get(student=self, object_id=question.id, content_type__model='mcquestion')
        if a_sheet.editable:
          a_sheet.answer = answer.text
          a_sheet.score = score
      except ObjectDoesNotExist:
        a_sheet = AnswerSheet(
          student=self,
          quiz=question.quiz,
          question=question,
          answer=answer.text,
          score=score
        )
      a_sheet.save()
      return a_sheet
    else:
      try:
        a_sheet = AnswerSheet.objects.get(student=self, object_id=question.id, content_type__model='subjectivequestion')
        if a_sheet.editable:
          a_sheet.answer = answer
      except ObjectDoesNotExist:
        a_sheet = AnswerSheet(
          student=self,
          quiz=question.quiz,
          question=question,
          answer=answer,
          score=None
        )
      a_sheet.save()
      return a_sheet

  def set_quiz_uneditable(self, quiz):
    AnswerSheet.objects.filter(student=self).filter(quiz=quiz).update(editable=False)


class AnswerSheet(models.Model):
  student = models.ForeignKey(Student)
  quiz = models.ForeignKey(Quiz)

  content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
  object_id = models.PositiveIntegerField()
  question = GenericForeignKey('content_type', 'object_id')

  answer = models.CharField(max_length=1000)
  score = models.IntegerField(default=0, null=True)

  editable = models.BooleanField(default=True)

  created_at = models.DateTimeField(default=timezone.now)
  updated_at = models.DateTimeField(default=timezone.now)

  def is_correct(self):
    if self.content_type.model == 'MCQuestion':
      return self.answer == self.question.answer.text
    else:
      return None

  def is_unscored(self):
    if self.content_type.model == 'SubjectiveQuestion':
      return self.score is None
    else:
      return None
