from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin


class AdminMixin(LoginRequiredMixin, UserPassesTestMixin):

  def test_func(self):
    return self.request.user.is_superuser


class TeacherMixin(LoginRequiredMixin, UserPassesTestMixin):

  def test_func(self):
    return hasattr(self.request.user, 'teacher')


class StudentMixin(LoginRequiredMixin, UserPassesTestMixin):
  def test_func(self):
    return hasattr(self.request.user, 'student')