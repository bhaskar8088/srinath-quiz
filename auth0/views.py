from django.contrib.auth import authenticate, login, logout, password_validation
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.views.generic import RedirectView, FormView

# Create your views here.
from auth0.forms import LoginForm, StudentRegisterForm
from quiz.models import School


class LoginView(FormView):
  template_name = 'auth0/login.html'
  form_class = LoginForm

  def get_context_data(self, **kwargs):
    context = super(LoginView, self).get_context_data(**kwargs)
    if self.request.GET.get('next', None):
      context['next_url'] = self.request.GET.get('next')
    return context

  def form_valid(self, form):
    login(self.request, form.get_user())
    if self.request.POST.get('next_url', None):
      self.success_url = self.request.POST.get('next_url')
    else:
      self.success_url = reverse_lazy('quiz:home')
    return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView):
  pattern_name = 'auth0:login'

  def get_redirect_url(self, *args, **kwargs):
    if self.request.user.is_authenticated():
      logout(self.request)
    return super(LogoutView, self).get_redirect_url(*args, **kwargs)


class RegisterView(FormView):
  template_name = 'auth0/register.html'
  form_class = StudentRegisterForm
  success_url = reverse_lazy('quiz:home')

  def get_context_data(self, **kwargs):
    context = super(RegisterView, self).get_context_data()
    context['schools'] = School.objects.all().order_by('name')
    return context

  def form_valid(self, form):
    form.save()
    login(self.request, form.get_user())
    return super(RegisterView, self).form_valid(form)


