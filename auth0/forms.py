from django import forms
from django.contrib.auth import authenticate, password_validation, get_user_model
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import capfirst

from quiz.models import Student, School, Teacher


class LoginForm(forms.Form):
  username = forms.EmailField()
  password = forms.CharField(widget=forms.PasswordInput())

  error_messages = {
    'invalid_login': ("Please enter a correct %(username)s and password. "
                      "Note that both fields may be case-sensitive."),
    'inactive': ("This account is inactive."),
  }

  def __init__(self, request=None, *args, **kwargs):
    """
    The 'request' parameter is set for custom auth use by subclasses.
    The form data comes in via the standard 'data' kwarg.
    """
    self.request = request
    self.user_cache = None
    super(LoginForm, self).__init__(*args, **kwargs)

    # Set the label for the "username" field.
    UserModel = get_user_model()
    self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
    if self.fields['username'].label is None:
      self.fields['username'].label = capfirst(self.username_field.verbose_name)

  class Meta:
    model = User
    fields = ('username', 'password',)

  def confirm_login_allowed(self, user):
    """
    Controls whether the given User may log in. This is a policy setting,
    independent of end-user authentication. This default behavior is to
    allow login by active users, and reject login by inactive users.

    If the given user cannot log in, this method should raise a
    ``forms.ValidationError``.

    If the given user may log in, this method should return None.
    """
    if not user.is_active:
      raise forms.ValidationError(
        self.error_messages['inactive'],
        code='inactive',
      )

  def clean(self):
    username = self.cleaned_data.get('username')
    password = self.cleaned_data.get('password')

    if username and password:
      self.user_cache = authenticate(username=username,
                                     password=password)
      if self.user_cache is None:
        raise forms.ValidationError(
          self.error_messages['invalid_login'],
          code='invalid_login',
          params={'username': self.username_field.verbose_name},
        )
      else:
        self.confirm_login_allowed(self.user_cache)

    return self.cleaned_data

  def get_user_id(self):
    if self.user_cache:
      return self.user_cache.id
    return None

  def get_user(self):
    return self.user_cache


class StudentRegisterForm(forms.ModelForm):
  error_messages = {
    'password_mismatch': ("The two password fields didn't match."),
  }

  first_name = forms.CharField(max_length=20)
  last_name = forms.CharField(max_length=20)
  middle_name = forms.CharField(max_length=20, required=False)
  username = forms.EmailField()

  school = forms.IntegerField()
  roll_num = forms.IntegerField()
  standard = forms.IntegerField()

  password1 = forms.CharField(label=("Password"),
                              strip=False,
                              widget=forms.PasswordInput)
  password2 = forms.CharField(label=("Password confirmation"),
                              widget=forms.PasswordInput,
                              strip=False,
                              help_text=("Enter the same password as before, for verification."))

  class Meta:
    model = User
    fields = ("first_name", "last_name","middle_name", "username", "password1", "password2")

  def __init__(self, request=None, *args, **kwargs):
    """
    The 'request' parameter is set for custom auth use by subclasses.
    The form data comes in via the standard 'data' kwarg.
    """
    self.request = request
    self.user_cache = None
    super(StudentRegisterForm, self).__init__(*args, **kwargs)

  def clean_password2(self):
    password1 = self.cleaned_data.get("password1")
    password2 = self.cleaned_data.get("password2")
    if password1 and password2 and password1 != password2:
      raise forms.ValidationError(
        self.error_messages['password_mismatch'],
        code='password_mismatch',
      )
    self.instance.username = self.cleaned_data.get('username')
    password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
    return password2

  def clean_school(self):
    try:
      return School.objects.get(id=self.cleaned_data.get("school"))
    except ObjectDoesNotExist:
      raise forms.ValidationError(
        self.error_messages['School does not exist'],
        code='does_not_exist',
      )

  def save(self, commit=True):
    user = super(StudentRegisterForm, self).save(commit=False)
    user.set_password(self.cleaned_data["password1"])
    if commit:
      user.save()
      student = Student(
        user=user,
        first_name=self.cleaned_data['first_name'],
        last_name=self.cleaned_data['last_name'],
        middle_name=self.cleaned_data['middle_name'],
        school=self.cleaned_data['school'],
        standard=self.cleaned_data['standard'],
        roll_num=self.cleaned_data['roll_num'],
      )
      student.save()

    self.user_cache = authenticate(username=self.cleaned_data.get('username'), password=self.cleaned_data.get('password2'))
    return user

  def get_user(self):
    return self.user_cache


class TeacherRegisterForm(forms.ModelForm):
  error_messages = {
    'password_mismatch': ("The two password fields didn't match."),
  }

  first_name = forms.CharField(max_length=20)
  last_name = forms.CharField(max_length=20)
  middle_name = forms.CharField(max_length=20, required=False)
  username = forms.EmailField()

  school = forms.IntegerField()
  standard = forms.IntegerField()

  password1 = forms.CharField(label=("Password"),
                              strip=False,
                              widget=forms.PasswordInput)
  password2 = forms.CharField(label=("Password confirmation"),
                              widget=forms.PasswordInput,
                              strip=False,
                              help_text=("Enter the same password as before, for verification."))

  class Meta:
    model = User
    fields = ("first_name", "last_name","middle_name", "username", "password1", "password2")

  def __init__(self, request=None, *args, **kwargs):
    """
    The 'request' parameter is set for custom auth use by subclasses.
    The form data comes in via the standard 'data' kwarg.
    """
    self.request = request
    self.user_cache = None
    super(TeacherRegisterForm, self).__init__(*args, **kwargs)

  def clean_password2(self):
    password1 = self.cleaned_data.get("password1")
    password2 = self.cleaned_data.get("password2")
    if password1 and password2 and password1 != password2:
      raise forms.ValidationError(
        self.error_messages['password_mismatch'],
        code='password_mismatch',
      )
    self.instance.username = self.cleaned_data.get('username')
    password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
    return password2

  def clean_school(self):
    try:
      return School.objects.get(id=self.cleaned_data.get("school"))
    except ObjectDoesNotExist:
      raise forms.ValidationError(
        self.error_messages['School does not exist'],
        code='does_not_exist',
      )

  def save(self, commit=True):
    user = super(TeacherRegisterForm, self).save(commit=False)
    user.set_password(self.cleaned_data["password1"])
    if commit:
      user.save()
      teacher = Teacher(
        user=user,
        first_name=self.cleaned_data['first_name'],
        last_name=self.cleaned_data['last_name'],
        middle_name=self.cleaned_data['middle_name'],
        school=self.cleaned_data['school'],
        standard=self.cleaned_data['standard'],
      )
      teacher.save()

    self.user_cache = authenticate(username=self.cleaned_data.get('username'), password=self.cleaned_data.get('password2'))
    return user

  def get_user(self):
    return self.user_cache